use x_log::*;

fn main() {
    // not necessary
    x_log::init_once(Some(Options {
        level: LevelFilter::Debug,
        print_level: true,
        timezone: Timezone::Local,
        colored: true,
        ..Default::default()
    }));

    let v1 = "123";
    let v2 = vec![4,5,6];

    info!("print some values below.");
    valn!(v1, v2);
}