use x_log::*;

fn main() {
    let v1: Vec<i32> = vec![1, 2, 3];
    let v2: Vec<i32> = vec![4, 5, 6];
    let v3 = "i am a string";
    let n = v1.len();

    val!(v1, v2, v3);
    valn!(v1, v2, v3);
    valf!(v1, v2, v3, n);
}