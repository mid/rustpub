use x_log::*;

#[derive(Debug)]
struct Position {
    x: f32,
    y: f32
}

fn main() {
    let f = |_: x_log::Line| {
        println!("this is a callback")
    };

    // you can also delete this
    x_log::init_once(Some(Options {
        print_level: true,
        timezone: Timezone::Local,
        colored: true,
        notify: Some(Box::new(f)),
        ..Default::default()
    }));

    let file = file!();

    trace!("This is an example message. file -> {}", file);
    debug!("This is an example message.");
    info!("This is an example message.");
    warn!("This is an example message.");
    error!("This is an example message.");

    let pos = Position { x: 3.234, y: -1.223 };

    info!("x is {} and y is {}",
       if pos.x >= 0.0 { "positive" } else { "negative" },
       if pos.y >= 0.0 { "positive" } else { "negative" });

    valf!(pos, pos);
    valf!(pos, pos);
}

