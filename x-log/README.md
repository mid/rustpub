## Log for both native and wasm

### common log functionality
* outputs:
  ```text
  2022-04-15 02:09:50 test:7 [T]: This is an example message
  ```
### print value functionality
* print values to console conveniently like this:

  `val!(v1, v2, v3);`

  output:
  ```text
  2022-04-21 22:19:59 val_print:26 
  v1︎︎︎︎⏤►[1, 2, 3]
  v2︎︎︎︎⏤►[4, 5, 6]
  v3︎︎︎︎⏤►"i am a string"
  ```

* or all in one line

  `valn!(v1, v2, v3);`

  them output:
  ```text
  2022-04-21 22:19:59 val_print:23 v1︎︎︎︎⏤►[1, 2, 3], n︎︎︎︎⏤►3, v2︎︎︎︎⏤►[4, 5, 6]
  ```
  
* or with pretty format

  `valf!(v1);`

  output:

  ```test
  2022-04-22 22:00:32 val_print:12 
  pos︎︎︎︎⏤►Position {
    x: 3.234,
    y: -1.223,
  }
  ```
### usage

```rust
use x_log::*;

fn main() {
  info!("This is an example message.");
}
```
or with config
```rust
use x_log::*;

fn main() {
  // not necessary
  x_log::init_once(Some(Options {
    level: LevelFilter::Debug,
    print_level: true,
    timezone: Timezone::Local,
    colored: true,
    ..Default::default()
  }));
  
  let v1 = "123";
  let v2 = vec![4,5,6];
  
  info!("print some values below.");
  val!(v1, v2);
}

```
### wasm
If you want to use it in wasm, enable feature `wasm`.