// use crate::*;
use x_utils::*;
use x_log::*;

fn main() {
    let m = map!(1 => "one", 2 => "xx");
    info!("map -> {:#?}", m);
    let x = m.get(&2).unwrap_or(&"123");

    let s = set!(3, 1, 2);
    error!("set -> {:#?}", s);
}